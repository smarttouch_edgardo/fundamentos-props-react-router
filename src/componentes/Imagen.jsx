import React from 'react'

const Imagen = (props) => {
  
  return (
    <img src={props.url} alt='Imagen'></img>
  )
}

export default Imagen