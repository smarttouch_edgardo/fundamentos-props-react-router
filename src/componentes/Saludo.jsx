import React from 'react'

const Saludo = (props) => {
  return (
    <div>
      <h2>Saludando a {props.nombre}</h2>
    </div>
  )
}

export default Saludo