import React from 'react'
import Imagen from './Imagen'

const Comentario = (props) => {
  

  return (
    <div>
        <Imagen url={props.rutaImagen}/>
        <div>
          <h2>{props.nombre}</h2>
          <h3>{props.texto}</h3>
        </div>
    </div>
  )
}

export default Comentario