import './App.css';
import Contacto from './componentes/Contacto';
import Tribus from './componentes/Tribus';
import Tribu from './componentes/Tribu';
import Inicio from './componentes/Inicio';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link/*,
  NavLink*/
} from 'react-router-dom'

function App() {

  //const rutaImagen = 'https://picsum.photos/200'
  
  return (
    <Router>
      <div className="container mt-5">
        <div className='btn-group'>
          <Link to="/" className='btn btn-dark'>Inicio</Link>
          <Link to="/tribus" className='btn btn-dark'>Tribus</Link>
          <Link to="/contacto" className='btn btn-dark'>Contacto</Link>
          
        </div>
        <hr/>
        <Switch>
          <Route path="/tribus/:id"><Tribu/></Route>
          <Route path="/contacto"> <Contacto/></Route>
          <Route path="/tribus"> <Tribus/></Route>
          <Route path="/"> <Inicio/></Route>
        </Switch>
      </div>
    </Router>
    
  );
}

export default App;
